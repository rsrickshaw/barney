![Barney Sastrup](barney.jpeg)

# Barney

Barney is a library of various C++ functions that make C++ programming a little bit less painful. These functions are designed to be fairly idiot-proof and many will try to auto correct if given unreasonable parameters.

To use Barney in your project, just add the barney.h file to your project directory and

```c++
#include "barney.h"
```

### Notes

* Barney includes the following dependencies: cstdlib, iostream, math.h, sstream, string, thread, vector.
* To avoid conflicts, make sure you do not start any identifier names with "\_\_barney\_".
* If you do not wish to include the entire header file, you can also copy the individual functions that you need, but note that some functions are dependent on others.
* The functions in the source code are sorted in the same order as they appear here.

### Contents

* [Debugging](#Debugging)
* [Printing](#Printing)
    * [print](#print)
    * [puts](#puts)
    * [abort](#abort)
* [Control Statements](#Control-Statements)
    * [unless](#unless)
    * [until](#until)
    * [forever](#forever)
    * [forin](#forin)
    * [times](#times)
* [Exceptions](#Exceptions)
* [Ranges](#Ranges)
    * [range](#range)
    * [range_](#range_)
* [Strings](#Strings)
    * [to_string](#to_string)
    * [slice](#slice)
    * [split](#split)
    * [join](#join)
    * [replace](#replace)
    * [strip](#strip)
    * [index](#index)
    * [lastindex](#lastindex)
    * [indices](#indices)
    * [includes](#includes)
    * [startswith](#startswith)
    * [endswith](#endswith)
    * [repeat](#repeat)
    * [uppercase](#uppercase)
    * [lowercase](#lowercase)
* [Vectors](#Vectors)
    * [slice](#slice)
    * [splice](#splice)
    * [index](#index-1)
    * [lastindex](#lastindex-1)
    * [indices](#indices-1)
    * [includes](#includes-1)
* [Miscellaneous](#Miscellaneous)
    * [fileread](#fileread)
    * [filewrite](#filewrite)
    * [eraser](#eraser)
    * [itoa](#itoa)
    * [getArguments](#getArguments)

## Debugging

The debug function prints a debug message corresponding to a given variable including the line number, the variable name, and the variable value. It can be toggled on and off by running "DEBUGON" and "DEBUGOFF" respectively.

```c++
DEBUGON;

int test = 5;
debug(test); // Prints "DEBUG (3) test = 5"

DEBUGOFF;

debug(test); // Does nothing

```

## Printing

Barney includes three printing functions inspired by Ruby. They each work without parenthesis on any input that can be printed through "cout". To print multiple things, use commas between each item.

### print

The print function prints the input as is.

```c++
print "hello";
print "world";

// Prints "helloworld"
```

### puts

The puts function prints the input and appends a newline.

```c++
puts "hello";
puts "world";

// Prints "hello
// world"
```

### abort

The abort function prints the input to STDERR, appends a newline, and terminates the program with the EXIT_FAILURE status.

```c++
abort "error";

// Prints "error
// " and stops
```

## Control Statements

These macros can replace commonly used control statements.

### unless

"unless()" is the opposite of "if()".

```c++
unless(var) {
  // Runs unless "var" is true.
}
```

### until

"until()" is the opposite of "while()".

```c++
until(var) {
  // Runs until "var" is true.
}
```

### forever

"forever" creates an infinite loop.

```c++
forever {
  // Runs forever unless "break" is called.
}
```

### forin

"forin" creates a ranged for loop.

```c++
forin(i, list) {
  // Iterate over each item "i" in "list".
}
```

### times

"times" creates a loop that runs for the specified number of times.

```c++
times(8) {
  // Runs eight times.
}
```

## Exceptions

The "begintry" and "retry" keywords from Ruby ("begin" conflicts with some built in functions so it had to be changed) can be used for exception handling. "begintry" is used in place of "try" and running "retry" will return the program back to the "begintry" keyword. "catch" is used normally.

```c++
vector<int> list {1, 2, 3};

begintry {
  list.at(3); // Trying to access the 4th item.
}
catch(...) {
  list.push_back(4); // Adding an item and trying again.
  retry;
}
```

## Ranges

### range

Inspired by Python, the range function produces a vector of integers given upper and lower limits and a step amount.

```c++
range(5);          // {0, 1, 2, 3, 4}
range(3, 6);       // {3, 4, 5}
range(4, 10, 2);   // {4, 6, 8}
range(0, -10, -2); // {0, 2, -4, -6, -8}
```

The vector that is produced can be used in ranged for loops and can be used to replace conventional for loops.

```c++
// Instead of...
for(int i = 0; i < size; i++);

// You can use...
for(int i : range(size));
```

The range function can also find the range between two characters, like in Ruby.

```c++
range('a', 'e'); // {'a', 'b', 'c', 'd'}
```

### range_

The range_ function does the same as range, but includes the last item.

```c++
range_(5);        // {0, 1, 2, 3, 4, 5}
range_('a', 'e'); // {'a', 'b', 'c', 'd', 'e'}
```

## Strings

These string manipulation functions are inspired by the string manipulation functions in JavaScript and Python. They do not modify the original string.

### to_string

The to_string function converts any type that can be printed via cout to a string.

```c++
to_string(1);                  // "1"
to_string('a');                // "a"
to_string("Already a string"); // "Already a string"
```

### slice

The slice function gets a string between two indices. If only one index is given, it gets the entire string from the starting index. If given a negative index, it will select from the end.

```c++
slice("Hello world!", 1, 4); // "ell"
slice("Hello world!", 1);    // "ello world!"
```

### split

The split function splits a string into a vector of strings given a string or character as a delimiter. Delimiters can be multiple characters in length.

```c++
split("How are you doing today?", " "); // {"How", "are", "you", "doing", "today?"}
```

### join

The join function joins a vector of strings into a string given a string or character as a delimiter. Delimiters can be multiple characters in length.

```c++
join({"How", "are", "you", "doing", "today?"}, " "); // "How are you doing today?";
```

### replace

The replace function replaces all instances of one string or character with another string or character. Leaving the third parameter blank will delete all instances of the first string.

```c++
replace("I have a blue house and a blue car", "blue", "red"); // "I have a red house and a red car"
replace("I have a blue house and a blue car", "blue");        // "I have a house and a car"
```

### strip

The strip function removes all leading and trailing whitespace.

```c++
strip("\n Hello World! \n"); // "Hello World!"
```

### index

The index function finds the index of the first occurence of a character or string. It returns -1 if no matches are found.

```c++
index("Hello world, welcome to the universe.", "e"); // 1
index("Hello world, welcome to the universe.", "z"); // -1
```

### lastindex

The lastindex function finds the index of the last occurence of a character or string. It returns -1 if no matches are found.

```c++
lastindex("Hello world, welcome to the universe.", "e"); // 35
lastindex("Hello world, welcome to the universe.", "z"); // -1
```

### indices

The indices function finds the indices of all occurences of a character or string. It returns a vector.

```c++
indices("Hello world, welcome to the universe.", "e"); // {1, 14, 19, 26, 32, 35}
indices("Hello world, welcome to the universe.", "z"); // {}
```

### includes

The includes function returns whether or not a string contains a given string or character.

```c++
includes("Hello world", "world"); // true
includes("Hello world", "test");  // false
```

### startswith

The startswith function returns whether a string starts with a given string or character.

```c++
startswith("Hello world", "Hello"); // true
startswith("Hello world", "world"); // false
```

### endswith

The endswith function returns whether a string ends with a given string or character.

```c++
endswith("Hello world", "world"); // true
endswith("Hello world", "Hello"); // false
```

### repeat

The repeat function produces a string containing a specified number of repetitions of a string or character.

```c++
repeat("test", 3); // "testtesttest"
```

### uppercase

The uppercase converts a string to uppercase.

```c++
uppercase("Hello World!"); // "HELLO WORLD!"
```

### lowercase

The lowercase converts a string to lowercase.

```c++
lowercase("Hello World!"); // "hello world!"
```

## Vectors

These vector manipulation functions are inspired by the array manipulation functions in JavaScript and offer similar functionality to the aforementioned string functions. They do not modify the original vector.

### slice

The slice function returns the part of a vector between two indices. If only one index is given, it gets the entire vector from the starting index. If given a negative index, it will select from the end.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
slice(example, 1, 3); // {"Orange", "Lemon"}
```

### splice

The splice function adds specified items and/or removes a specified number of items from a vector at a given index. If given a negative index, it will select from the end.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
splice(example, 2, 1); // {"Banana", "Orange", "Apple", "Mango"}

vector<string> additions {"Pineapple", "Kiwi"};
splice(example, 2, 1, additions); // {"Banana", "Orange", "Pineapple", "Kiwi", "Apple", "Mango"}
splice(example, 2, additions);    // {"Banana", "Orange", "Pineapple", "Kiwi", "Lemon", "Apple", "Mango"}
```

### index

The index function finds the index of the first occurence of an item. It returns -1 if no matches are found.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
index(example, string("Orange")); // 1
index(example, string("Carrot")); // -1
```

### lastindex

The lastindex function finds the index of the last occurence of an item. It returns -1 if no matches are found.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
lastindex(example, string("Apple"));  // 3
lastindex(example, string("Carrot")); // -1
```

### indices

The indices function finds the indices of all occurences of an item. It returns a vector.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
indices(example, string("Apple"));  // {3}
indices(example, string("Carrot")); // {}
```

### includes

The includes function returns whether or not a vector contains a given item.

```c++
vector<string> example {"Banana", "Orange", "Lemon", "Apple", "Mango"};
includes(example, "Orange"); // true
includes(example, "Tomato"); // false
```

## Miscellaneous

### fileread
The fileread function reads an entire file into a string. If the file does not exist, it will return an empty string. It does not modify the file.

```c++
string filecontents = fileread("example.txt");
```

### filewrite
The filewrite function writes a string to a file. If the file exists it will be overwritten and if the files does not exist it will be created.

```c++
filewrite("example.txt", "Hello world");
```

### eraser
The eraser function removes items from an STL container if they equal a given value or match a given condition. It modifies the original container.

```c++
vector<int> list {1, 2, 3, 4, 5};
eraser(list, 4); // list = {1, 2, 3, 5};

bool odd(int n) { return n % 2; } // Returns whether a number is odd
vector<int> list2 {1, 2, 3, 4, 5};
eraser(list, odd); // list = {2, 4}

```

### itoa

The itoa function converts an integer to a char pointer.

### getArguments

The getArguments function converts the "argc" and "argv" values passed into the main function into a vector of strings.

### More coming soon 😎

![Barney Sastrup](barney2.jpg)
