// Various C++ functions that make C++ programming a little bit less painful
// https://github.com/rsrickshaw/barney

#ifndef BARNEY
#define BARNEY

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <math.h>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

// Debugging

bool __barney_debug = false;

void __barney_debugon() { __barney_debug = true; }
void __barney_debugoff() { __barney_debug = false; }

#define DEBUGON __barney_debugon()

#define DEBUGOFF __barney_debugoff()

#define debug(a)                                                                  \
  {                                                                               \
    if(__barney_debug)                                                            \
      std::cout << "DEBUG (" << __LINE__ << ") " << #a " = " << (a) << std::endl; \
  }

// Printing

#define print __barney_print,
#define puts __barney_puts,
#define abort __barney_abort,

std::stringstream __barney_print;
std::stringstream __barney_puts;
std::stringstream __barney_abort;

template<class T>
inline std::ostream& operator,(std::ostream& o, const T& value) {
  if(&o == &__barney_print)
    std::cout << value;
  else if(&o == &__barney_puts)
    std::cout << value << std::endl;
  else if(&o == &__barney_abort) {
    std::cerr << value << std::endl;
    std::exit(EXIT_FAILURE);
  }
  return o;
}

inline std::ostream& operator,(std::ostream& o, std::ostream& (*manip_fun)(std::ostream&)) {
  o << manip_fun;
  return o;
}

// Loops

#define unless(...) if(not(__VA_ARGS__))
#define until(...) while(not(__VA_ARGS__))
#define forever while(true)
#define forin(a, b) for(auto& a : b)
#define times(a) for(int __barney_index = 0; __barney_index < a; __barney_index++)

// Exceptions

#define begintry                                                                         \
  for(int __barney_retryhandler = 0; __barney_retryhandler < 1; __barney_retryhandler++) \
    try

#define retry                \
  {                          \
    __barney_retryhandler--; \
    continue;                \
  }

// Ranges

std::vector<int> range(int lower, int upper, int increment) {
  std::vector<int> result;
  if(increment > 0)
    for(int i = lower; i < upper; i += increment)
      result.push_back(i);
  else
    for(int i = lower; i > upper; i += increment)
      result.push_back(i);
  return result;
}

std::vector<int> range(int lower, int upper) {
  if(upper > lower)
    return range(lower, upper, 1);
  else
    return range(lower, upper, -1);
}

std::vector<int> range(int upper) {
  return range(0, upper);
}

std::vector<char> range(char lower, char upper) {
  std::vector<char> result;
  if(upper > lower)
    for(char i = lower; i < upper; i++)
      result.push_back(i);
  else
    for(char i = lower; i > upper; i--)
      result.push_back(i);
  return result;
}

std::vector<int> range_(int lower, int upper, int increment) {
  std::vector<int> result;
  if(increment > 0)
    for(int i = lower; i <= upper; i += increment)
      result.push_back(i);
  else
    for(int i = lower; i >= upper; i += increment)
      result.push_back(i);
  return result;
}

std::vector<int> range_(int lower, int upper) {
  if(upper > lower)
    return range_(lower, upper, 1);
  else
    return range_(lower, upper, -1);
}

std::vector<int> range_(int upper) {
  return range_(0, upper);
}

std::vector<char> range_(char lower, char upper) {
  std::vector<char> result;
  if(upper > lower)
    for(char i = lower; i <= upper; i++)
      result.push_back(i);
  else
    for(char i = lower; i >= upper; i--)
      result.push_back(i);
  return result;
}

// Strings

template<class T>
std::string to_string(const T& t) {
  std::ostringstream os;
  os << t;
  return os.str();
}

std::string slice(std::string base, int start, int finish) {
  if(start < 0)
    start = base.size() + start;
  else if(start > base.size())
    start = base.size();
  if(finish < 0)
    finish = base.size() + finish;
  else if(finish > base.size())
    finish = base.size();
  if(start > finish) {
    int swap = start;
    start    = finish;
    finish   = swap;
  }
  return base.substr(start, finish - start);
}

std::string slice(std::string base, int start) {
  return slice(base, start, base.length());
}

std::vector<std::string> split(std::string base, std::string delimiter) {
  std::vector<std::string> result;
  int                      index = 0;
  std::string              current;
  while(index <= base.length() - delimiter.length()) {
    if(base.substr(index, delimiter.length()) == delimiter) {
      if(current.length())
        result.push_back(current);
      current = "";
      index += delimiter.length();
      continue;
    }
    current += base.at(index);
    index++;
  }
  if(current.length())
    result.push_back(current);
  return result;
}

std::vector<std::string> split(std::string base, char delimiter) {
  return split(base, std::string(1, delimiter));
}

std::string join(std::vector<std::string> base, std::string delimiter) {
  std::string result;
  for(int i = 0; i < base.size() - 1; i++)
    result += base.at(i) + delimiter;
  result += base.at(base.size() - 1);
  return result;
}

std::string join(std::vector<std::string> base, char delimiter) {
  return join(base, std::string(1, delimiter));
}

std::string replace(std::string base, std::string oldstring, std::string newstring) {
  int         index = 0;
  std::string current;
  while(index <= base.length() - oldstring.length()) {
    if(base.substr(index, oldstring.length()) == oldstring) {
      current += newstring;
      index += oldstring.length();
      continue;
    }
    current += base.at(index);
    index++;
  }
  return current;
}

std::string replace(std::string base, char oldstring, std::string newstring) {
  return replace(base, std::string(1, oldstring), newstring);
}

std::string replace(std::string base, std::string oldstring, char newstring) {
  return replace(base, oldstring, std::string(1, newstring));
}

std::string replace(std::string base, char oldstring, char newstring) {
  return replace(base, std::string(1, oldstring), std::string(1, newstring));
}

std::string replace(std::string base, std::string oldstring) {
  return replace(base, oldstring, "");
}

std::string replace(std::string base, char oldstring) {
  return replace(base, std::string(1, oldstring), "");
}

std::string strip(std::string base) {
  if(base.length() == 0)
    return "";
  int start  = 0;
  int finish = base.length() - 1;
  while(isspace(base.at(start)))
    start++;
  while(isspace(base.at(finish)))
    finish--;
  return base.substr(start, finish + 1 - start);
}

int index(std::string base, std::string target) {
  int index = 0;
  while(index + target.length() <= base.length()) {
    if(base.substr(index, target.length()) == target)
      return index;
    index++;
  }
  return -1;
}

int index(std::string base, char target) {
  return index(base, std::string(1, target));
}

int lastindex(std::string base, std::string target) {
  int index = base.length() - target.length();
  while(index) {
    if(base.substr(index, target.length()) == target)
      return index;
    index--;
  }
  return -1;
}

int lastindex(std::string base, char target) {
  return lastindex(base, std::string(1, target));
}

std::vector<int> indices(std::string base, std::string target) {
  std::vector<int> indices;
  int              index = 0;
  while(index + target.length() <= base.length()) {
    if(base.substr(index, target.length()) == target)
      indices.push_back(index);
    index++;
  }
  return indices;
}

std::vector<int> indices(std::string base, char target) {
  return indices(base, std::string(1, target));
}

bool includes(std::string base, std::string target) {
  return (index(base, target) > -1);
}

bool includes(std::string base, char target) {
  return includes(base, std::string(1, target));
}

bool startswith(std::string base, std::string target) {
  if(base.length() >= target.length())
    return (base.substr(0, target.length()) == target);
  return false;
}

bool startswith(std::string base, char target) {
  return startswith(base, std::string(1, target));
}

bool endswith(std::string base, std::string target) {
  if(base.length() >= target.length())
    return (base.substr(base.length() - target.length()) == target);
  return false;
}

bool endswith(std::string base, char target) {
  return endswith(base, std::string(1, target));
}

std::string repeat(std::string base, int repetitions) {
  std::string result;
  for(int i = 0; i < repetitions; i++)
    result += base;
  return result;
}

std::string repeat(char base, int repetitions) {
  return repeat(std::string(1, base), repetitions);
}

std::string uppercase(std::string base) {
  std::string result = base;
  for(int i = 0; i < base.size(); i++)
    if(base.at(i) >= 'a' and base.at(i) <= 'z')
      result.at(i) -= ('a' - 'A');
  return result;
}

std::string lowercase(std::string base) {
  std::string result = base;
  for(int i = 0; i < base.size(); i++)
    if(base.at(i) >= 'A' and base.at(i) <= 'Z')
      result.at(i) += ('a' - 'A');
  return result;
}

// Vectors

template<typename T>
std::vector<T> slice(std::vector<T> base, int start, int finish) {
  std::vector<T> result;
  if(start < 0)
    start = base.size() + start;
  else if(start > base.size())
    start = base.size();
  if(finish < 0)
    finish = base.size() + finish;
  else if(finish > base.size())
    finish = base.size();
  if(start > finish) {
    int swap = start;
    start    = finish;
    finish   = swap;
  }
  for(int i = start; i < finish; i++)
    result.push_back(base.at(i));
  return result;
}

template<typename T>
std::vector<T> slice(std::vector<T> base, int start) {
  return slice(base, start, base.size());
}

template<typename T>
std::vector<T> splice(std::vector<T> base, int start, int count, std::vector<T> additions) {
  std::vector<T> result;
  if(start < 0)
    start = base.size() + start;
  else if(start > base.size())
    start = base.size();
  if(count > base.size() - start)
    count = base.size() - start;
  for(int i = 0; i < start; i++)
    result.push_back(base.at(i));
  for(int i = 0; i < additions.size(); i++)
    result.push_back(additions.at(i));
  for(int i = start + count; i < base.size(); i++)
    result.push_back(base.at(i));
  return result;
}

template<typename T>
std::vector<T> splice(std::vector<T> base, int start, int count) {
  std::vector<T> empty;
  return splice(base, start, count, empty);
}

template<typename T>
std::vector<T> splice(std::vector<T> base, int start, std::vector<T> additions) {
  return splice(base, start, 0, additions);
}

template<typename T>
int index(std::vector<T> base, T target) {
  int index = 0;
  while(index < base.size()) {
    if(base.at(index) == target)
      return index;
    index++;
  }
  return -1;
}

template<typename T>
int lastindex(std::vector<T> base, T target) {
  int index = base.size();
  while(index) {
    if(base.at(index) == target)
      return index;
    index--;
  }
  return -1;
}

template<typename T>
std::vector<int> indices(std::vector<T> base, T target) {
  std::vector<int> indices;
  int              index = 0;
  while(index < base.size()) {
    if(base.at(index) == target)
      indices.push_back(index);
    index++;
  }
  return indices;
}

template<typename T>
bool includes(std::vector<T> base, T target) {
  return (index(base, target) > -1);
}

// Miscellaneous

std::string fileread(std::string filename) {
  std::stringstream content;
  std::ifstream     s;
  s.open(filename, std::ios::in);
  content << s.rdbuf();
  s.close();
  return content.str();
}

void filewrite(std::string filename, std::string content) {
  std::ofstream s;
  s.open(filename, std::ios::out | std::ios::trunc);
  s << content;
  s.close();
}

template<typename T>
void eraser(std::vector<T>& base, T target) {
  base.erase(std::remove(base.begin(), base.end(), target), base.end());
}

template<typename T>
void eraser(std::vector<T>& base, bool (*condition)(T)) {
  base.erase(std::remove_if(base.begin(), base.end(), condition), base.end());
}

char* itoa(long n) {
  int len = n == 0 ? 1 : floor(log10l(labs(n))) + 1;
  if(n < 0)
    len++;
  char* buf = (char*)calloc(sizeof(char), len + 1);
  snprintf(buf, len + 1, "%ld", n);
  return buf;
}

std::vector<std::string> getArguments(int argc, char** argv) {
  std::vector<std::string> arguments;
  for(int i = 0; i < argc; i++)
    arguments.push_back(std::string(argv[i]));
  return arguments;
}

#define ARGV getArguments(argc, argv)

#endif
